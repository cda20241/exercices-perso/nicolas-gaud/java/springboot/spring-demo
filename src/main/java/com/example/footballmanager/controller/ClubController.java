package com.example.footballmanager.controller;

import com.example.footballmanager.entity.ClubEntity;
import com.example.footballmanager.repository.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/club")
public class ClubController {
    // on utilise l'annotation Autowired ici pour déclarer que clubRepository sera injecté
    @Autowired
    private  final ClubRepository clubRepository;

    public ClubController(ClubRepository clubRepository) {
        // C'est Spring qui se charge d'instancier la classe ClubController, et en le faisant il voit
        // qu'il doit aussi créer une instance de ClubRepository et nous la passer ici
        // ça nous simplifie la vie plutôt que de devoir créer nous-même l'instance
        this.clubRepository = clubRepository;
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher tous les clubs de la base de donnée.
     * @return une liste de Clubs.
     */
    @GetMapping("/all")
    public List<ClubEntity> getAllClubs() {
        return clubRepository.findAll();
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un club selon son id.
     * @param id
     * @return Une instance Club
     */
    @GetMapping("/{id}")
    public ClubEntity getClubById(@PathVariable Long id) {
        Optional<ClubEntity> club = clubRepository.findById(id);
        if (club.isPresent()) {
            return club.get();
        }
        else {
            return null;
        }
        //return club.orElse(null);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour afficher un club selon son id.
     * @param nom
     * @return Une instance Club
     */
    @GetMapping("/name")
    public ClubEntity getClubByName(@RequestParam("nom") String nom) {
        Optional<ClubEntity> club = clubRepository.findByNom(nom);
        return club.orElse(null);
    }


    /**
     * Fonction du controller qui permet de recevoir la requête pour ajouter un club.
     * @param club
     * @return
     */
    @PostMapping("")
    public ResponseEntity<String> addclub(@RequestBody ClubEntity club){
        // On set l'id a nul pour ne pas remplacer de club, car on ne fait pas un update ici
        club.setId(null);
        club = this.clubRepository.save(club);
        return new ResponseEntity<>("Welcome to " + club.getNom() + " " + club.getVille() + ".", HttpStatus.CREATED);
    }

    /**
     * Fonction du controller qui permet de recevoir la requête pour modifier un club.
     * @param id
     * @param club
     * @return une instance de club modifiée
     */
    @PutMapping("/{id}")
    public ClubEntity updateClub(@PathVariable Long id, @RequestBody ClubEntity club) {
        club.setId(id);
        return this.clubRepository.save(club);
    }

    /**
     * Fonction du controller qui permet de recevoir la reqûete pour supprimer un club.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteClub(@PathVariable Long id) {
        // ici on doit utiliser le type Optional car on n'est pas sûr que le club avec l'id demandé existe en bdd
        // auquel cas s'il n'existe pas le repository nous renverra null
        Optional<ClubEntity> club = this.clubRepository.findById(id);
        if (club.isPresent()) {
            this.clubRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
    
}
