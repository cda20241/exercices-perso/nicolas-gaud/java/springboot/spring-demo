package com.example.footballmanager.repository;

import com.example.footballmanager.entity.ClubEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ClubRepository extends JpaRepository<ClubEntity,Long> {
    Optional<ClubEntity> findByNom(@Param("nom") String nom);
    // la class JoueurRepository hérite de JpaRepository
    // JpaRepository fournit déjà toutes les fonctions les plus courantes de requêtes à la base de données
    // la fonction findById(), findAll(), create() etc...
    // la plupart du temps on n'a donc rien à faire de plus dans le repository
    // on doit faire 1 repository par class entity
}
