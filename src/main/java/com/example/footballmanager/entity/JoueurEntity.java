package com.example.footballmanager.entity;

import jakarta.persistence.*;


// permet de définir que cette class est une class model (= entity) et donc que JPA doit s'occuper
// de mapper cette classe avec une table en base de données
@Entity
@Table(name="joueur")
public class JoueurEntity {
    // permet de déclarer que cette valeur est la clef primaire
    @Id
    // permet de dire qu'on veut générer l'ID par auto-incrément
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // permet de dire que cet attribut de la classe sera enregistrée comme colonne dans la table
    // on peut avoir des attributs dans la class que l'on ne déclare pas en Column et donc ils ne seront
    // pas présents dans la table en base
    @Column(name="id",nullable = false,updatable = false)
    private Long id;

    @Column
    private String nom;

    // on peut si on le souhaite ajouter des paramètres comme quand on créait nous-mêmes les colonnes en bdd
    // ici le string aura une longueur max
    @Column(length = 60)
    private  String prenom;


    /*********************** CONSTRUCTEUR *********************/
    // je génère le ou les constructeurs dont j'ai besoin

    public JoueurEntity() {
    }

    public JoueurEntity(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public JoueurEntity(Long id, String nom, String prenom) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
    }

    /*********************** GETTER et SETTER *********************/
    public java.lang.Long getId() {
        return id;
    }

    public java.lang.String getNom() {
        return nom;
    }

    public java.lang.String getPrenom() {
        return prenom;
    }

    public void setId(java.lang.Long id) {
        this.id = id;
    }

    public void setNom(java.lang.String nom) {
        this.nom = nom;
    }

    public void setPrenom(java.lang.String prenom) {
        this.prenom = prenom;
    }
}
