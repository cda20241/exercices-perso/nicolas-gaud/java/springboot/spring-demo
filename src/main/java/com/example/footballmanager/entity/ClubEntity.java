package com.example.footballmanager.entity;

import jakarta.persistence.*;


// permet de définir que cette class est une class model (= entity) et donc que JPA doit s'occuper
// de mapper cette classe avec une table en base de données
@Entity
@Table(name="club")
public class ClubEntity {
    // permet de déclarer que cette valeur est la clef primaire
    @Id
    // permet de dire qu'on veut générer l'ID par auto-incrément
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // permet de dire que cet attribut de la classe sera enregistrée comme colonne dans la table
    // on peut avoir des attributs dans la class que l'on ne déclare pas en Column et donc ils ne seront
    // pas présents dans la table en base
    @Column(name="id",nullable = false,updatable = false)
    private Long id;

    @Column
    private String nom;

    // on peut si on le souhaite ajouter des paramètres comme quand on créait nous-mêmes les colonnes en bdd
    // ici le string aura une longueur max
    @Column(length = 60)
    private  String ville;

    /*********************** CONSTRUCTEUR *********************/
    public ClubEntity() {
    }

    public ClubEntity(String nom, String ville) {
        this.nom = nom;
        this.ville = ville;
    }

    public ClubEntity(Long id, String nom, String ville) {
        this.id = id;
        this.nom = nom;
        this.ville = ville;
    }

    /*********************** GETTER et SETTER *********************/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }
}
